#include "vamp-hostsdk/RealTime.h"
#include <sys/time.h>
#include <iostream>

int main() {
  Vamp::RealTime t1 = Vamp::RealTime(0, 500000000);
  struct timeval tv;
  tv.tv_sec = 0; tv.tv_usec = 500000;
  Vamp::RealTime t2 = Vamp::RealTime::fromTimeval(tv);
  std::cout << "RealTime(0, ONE_BILLION/2) = " << t1 << std::endl;
  std::cout << "RealTime::fromTimeval(0, ONE_MILLION/2) = " << t2 << std::endl;

  if (t1 != t2)
	  return 1;

  return 0;
}
